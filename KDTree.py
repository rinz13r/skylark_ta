# My implementation of KDTree
# Would need to look at what SciPy has done and switch if needed (performance).

class Rectangle:
    def __init__ (self, c1, c2):
        '''
        ----*(c2)
        -----
    (c1)*----
        '''
        self._c1 = c1
        self._c2 = c2
    def intersection (self, point):
        x, y = point
        x1, y1 = self._c1
        x2, y2 = self._c2
        return (x1 <= x and x <= x2 and y1 <= y and y <= y2)

    def left (self, point):
        x, y = point
        x1, y1 = self._c1
        x2, y2 = self._c2
        return (y1 <= y and y <= y2 and x < x1)
    def right (self, point):
        x, y = point
        x1, y1 = self._c1
        x2, y2 = self._c2
        return (y1 <= y and y <= y2 and x > x2)
    def up (self, point):
        x, y = point
        x1, y1 = self._c1
        x2, y2 = self._c2
        return (x1 <= x and x <= x2 and y > y2)
    def down (self, point):
        x, y = point
        x1, y1 = self._c1
        x2, y2 = self._c2
        return (x1 <= x and x <= x2 and y < y1)

class KDTree:
    def __init__ (self, points):
        self._points = points
        self._dim = len (points[0])
        self._root = self._build (points)

    def _build (self, points, depth=0):
        n = len (points)
        if (n == 0):
            return None
        
        axis = depth%(self._dim)
        
        sorted_points = sorted (points, key=lambda point : point[axis])
        half = int(n/2)
        return {
            'point' : sorted_points[half],
            'left_child' : self._build (sorted_points[:half], depth+1),
            'right_child' : self._build (sorted_points[half+1:], depth+1)
        }
    
    def range_query (self, radius, point):
        x, y = point
        rect = Rectangle ((x-radius, y-radius), (x+radius,y+radius))
        return self._range_query (rect, self._root)
    
    def _range_query (self, rect, root, axis=0):
        if (root is None):
            return []
        point = root['point']
        left_child = root['left_child']
        right_child = root['right_child']
        if (rect.intersection (point)):
            res = [point]
            a = 0
            if (axis == 0):
                a = 1
            res += self._range_query (rect, left_child, a)
            res += self._range_query (rect, right_child, a)
            return  res
        elif (axis is 0):

            if (rect.left (point)):
                return self._range_query (rect, right_child, 1)
            elif (rect.right (point)):
                return self._range_query (rect, left_child, 1)
            return self._range_query (rect, right_child, 1) + self._range_query (rect, left_child, 1)
        else:
            if (rect.up (point)):
                return self._range_query (rect, left_child)
            elif (rect.down (point)):
                return self._range_query (rect, right_child)
            
            return self._range_query (rect, left_child) + self._range_query (rect, right_child)

        return []