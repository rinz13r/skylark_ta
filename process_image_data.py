import exifread
import utm

# Returns a dictionary containing {UTM Coordinate : image_path}
# Basically the images are being indexed by their UTM Coordinate.
def fetch_image_loc_data (image_path_list):
    d = {}

    for file_path in image_path_list:
        f = open (file_path, 'rb')
        tags = exifread.process_file (f)
        
        if ('GPS GPSLongitude' not in tags.keys () or 'GPS GPSLatitude' not in tags.keys()):
            # turns out some images (4) don't have GPS coordinate values in the description.
            continue
        
        long_list = [x.num/x.den for x in tags ['GPS GPSLongitude'].values]
        lat_list  = [x.num/x.den for x in tags ['GPS GPSLatitude'].values ]

        longitude = (long_list[2]/60 + long_list[1])/60 + long_list[0]
        latitude  = (lat_list[2]/60 + lat_list[1])/60 + lat_list[0]
        
        # Converting to utm for convenience.
        # Most of them are in the same zone : Number 43. So, the Euclidean Distance between the 
        # easting-northing pair is a good approximation of the actual value.

        # But this is something that has to be kept in mind in case the data is mixed with other zones.
        p = utm.from_latlon (latitude, longitude)
        x, y, _, _ = p
        d[(x, y)] = file_path

    return d