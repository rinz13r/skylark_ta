import srt

# Returns a dictionary containing {'sec' second: [list of drone locations (unique) for the second 'sec']}
def fetch_video_loc_data (srt_file_path):
    res = ''
    with open (srt_file_path) as srt_file:
        res = srt_file.read ()

    subs = list (srt.parse (res))
    d = {}
    s = set () # For uniqueness of locations
    for sub in subs:
        if (sub.start.microseconds == 0):
            d[sub.start.seconds - 1] = list (s)
            s = set ()
        else:
            x, y, _ = sub.content.split (',')
            s.add ((float(x), float(y)))
    return d