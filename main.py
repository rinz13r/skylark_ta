import csv
import math
import sys

from KDTree import KDTree
from process_image_data import fetch_image_loc_data, utm
from process_video_data import fetch_video_loc_data


if __name__ == '__main__':
    images_path_list = ['data/images/DJI_'+"{:0004}".format (num)+ '.JPG' for num in range (4, 608)]
    srt_file_path = 'data/videos/DJI_0301.SRT'
    d = fetch_image_loc_data (images_path_list)

    points = list (d.keys ())
    kd_tree = KDTree (points)

    # The range is to be passed as a command line argument.
    rad = float (sys.argv[1])

    with open ('result.csv', 'w') as csvfile:
        fieldnames = ['second', 'image']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        
        vl = fetch_video_loc_data (srt_file_path)
        for sec in vl.keys ():
            photos = set ()
            locs = vl[sec]
            for f in locs:
                p = utm.from_latlon (f[1], f[0])
                x, y, _, _ = p
                res = kd_tree.range_query (rad, (x, y))
                # My implementation of the KDTree supports querying in a rectangle, not a circle.
                # So, I need to check if the point actually lies withing the radius.
                # This doesn't affect the time complexity a lot since,
                # (4 - pi)/4 = 0.21
                for p in res:
                    dx, dy = x-p[0], y-p[1]
                    if (math.sqrt (dx*dx + dy*dy) <= rad):
                        photos.add (d[p])
            for el in photos:
                writer.writerow ({'second' : sec, 'image' : el})