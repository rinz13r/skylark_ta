# Steps to run
```
pip3 install -r requirements.txt
python3 main.py 'r'
```
where 'r' is the desired range.


# Approach
To find the images within r units of a given location, I used a KDTree.
KDTree has complexity O(n.log(n)) for construction and O(sqrt(n)) for range queries.