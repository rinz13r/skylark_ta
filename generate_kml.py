from process_video_data import fetch_video_loc_data

def generate_kml_file ():
    srt_file_path = 'data/videos/DJI_0301.SRT'
    f = fetch_video_loc_data (srt_file_path)

    opening_tags = '''
    <?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2">
    <Document>
    <Placemark>
    <LineString>
    <coordinates>
    '''
    closing_tags = '</coordinates></LineString></Placemark></Document></kml>'

    with open ('vid.kml', 'w') as kmlfile:
        kmlfile.write (opening_tags)
        for l in f.values ():
            for i in l:
                kmlfile.write (str(i[0]) + ', ' + str(i[1]) + '\n')
        kmlfile.write (closing_tags)

if __name__ == '__main__':
    generate_kml_file ()